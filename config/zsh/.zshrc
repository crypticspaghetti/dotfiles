#!/bin/zsh

autoload -Uz compinit

# Autocomplete hidden files
_comp_options+=(globdots)

source $ZDOTDIR/external/completion.zsh

fpath=($ASDF_DIR/completions $ZDOTDIR/external $fpath)
autoload -Uz cursor_mode; cursor_mode

# Don't nice background tasks
setopt NO_BG_NICE
setopt NO_HUP
setopt NO_BEEP
# Allow functions to have local options
setopt LOCAL_OPTIONS
# Allow functions to have loacl traps
setopt LOCAL_TRAPS
# Add timestampts to history
setopt EXTENDED_HISTORY
setopt PROMPT_SUBST
setopt CORRECT
setopt COMPLETE_IN_WORD
# Adds history
setopt APPEND_HISTORY
setopt INC_APPEND_HISTORY
setopt SHARE_HISTORY
# Don't record dupes in history
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_REDUCE_BLANKS
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_VERIFY
setopt HIST_EXPIRE_DUPS_FIRST
# Don't ask for confirmation in rm globs*
setopt RM_STAR_SILENT
# Push current directory on to the stack
setopt AUTO_PUSHD
# No duplicate directories on the stack
setopt PUSHD_IGNORE_DUPS
# No printing stack when using pushd or popd
setopt PUSHD_SILENT

# Use vim-like mode for menuselect while auto-completing
zmodload zsh/complist
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char

# Use $EDITOR to edit command-line prompt
autoload -Uz edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line

if [ $(command -v "fzf") ]; then
    source /usr/share/fzf/completion.zsh
    source /usr/share/fzf/key-bindings.zsh
fi

source $HOME/.scripts/fzf-git.sh

eval $(thefuck --alias)

eval "$(zoxide init zsh)"

eval "$(starship init zsh)"

# Rebind clear command
bindkey -r '^l'
bindkey -r '^l'
bindkey -s '^g' 'clear\n'

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

. "$HOME/.asdf/asdf.sh"
. "${ASDF_DIR}/installs/rust/1.82.0/env"

source $ZDOTDIR/scripts.zsh
source "$ZDOTDIR/aliases"

compinit
