-- Interacts with nvim's treesitter api to provide highlighting, code folding, etc. for languages
-- This plugin is experimental and not *really* needed

return {
	"nvim-treesitter/nvim-treesitter",
	build = ":TSUpdate",
	config = function()
		require("nvim-treesitter.configs").setup({
			ensure_installed = { "vim", "vimdoc", "lua", "html", "css", "javascript", "typescript" },
			sync_install = false,
			auto_install = true,
			highlight = {
				enable = true,
				additional_vim_regex_highlighting = true,
			},
			indent = { enable = true },
		})
	end,
}

-- vim: ts=2 sts=2 sw=2 et
