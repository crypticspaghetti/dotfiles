-- File tree

return {
	"nvim-neo-tree/neo-tree.nvim",
	branch = "v3.x",
	dependencies = {
		"nvim-lua/plenary.nvim",
		"nvim-tree/nvim-web-devicons",
		"MunifTanjim/nui.nvim",
	},
	keys = {
		{ "<C-n>", ":Neotree toggle left<cr>", desc = "Toggle neo-tree pane" },
	},
}

-- vim: ts=2 sts=2 sw=2 et
