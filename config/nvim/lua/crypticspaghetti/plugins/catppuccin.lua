-- Theme

return {
	"catppuccin/nvim",
	name = "catppuccin",
  lazy = false,
	priority = 1000,
	config = function()
		vim.cmd.colorscheme("catppuccin-macchiato")
	end,
}

-- vim: ts=2 sts=2 sw=2 et
