-- A startpage/dashboard when nvim opened in a directory

return {
	"goolord/alpha-nvim",
	dependencies = {
		"nvim-tree/nvim-web-devicons",
	},
	config = function()
		local alpha = require("alpha")
		local theta = require("alpha.themes.theta")
		local dashboard = require("alpha.themes.dashboard")
    local mru = theta.mru
		local config = theta.config
		local cdir = vim.fn.getcwd()

		local straw_hat_crew = [[
      ⠀⠀⠀⠀⢀⣤⣤⣤⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣤⣤⣤⡀⠀⠀⠀⠀
      ⠀⠀⠀⠀⣾⣿⣿⣿⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⣀⣤⣤⣤⣤⣄⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣾⣿⣿⣿⣷⠀⠀⠀⠀
      ⠀⣴⣶⣾⣿⣿⣿⣿⡋⠀⠀⠀⠀⠀⠀⠀⠀⣀⣤⣶⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⣤⣄⠀⠀⠀⠀⠀⠀⠀⠀⠙⣿⣿⣿⣿⣷⣶⣄⠀
      ⠀⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠀⠀⠀⠀⣠⣾⠏⢹⡿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠻⢷⣄⠀⠀⠀⠀⢀⣰⣿⣿⣿⣿⣿⣿⣿⣿⠀
      ⠀⠙⠻⠿⠛⠉⠻⣿⣿⣿⣿⣦⡀⢀⣼⣿⢁⣼⠏⣠⡟⢠⣿⣿⣿⣿⣿⣿⣿⣿⡀⢻⣷⡀⢿⣷⡀⢀⣴⣿⣿⣿⣿⠟⠉⠻⠿⠿⠋⠀
      ⠀⠀⠀⠀⠀⠀⠀⠈⠻⣿⣿⡿⠀⠈⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠁⠈⠉⠁⠀⢻⣿⣿⠟⠁⠀⠀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠻⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠛⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⣀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠃⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢿⣿⣿⣿⣿⠿⠋⠉⠉⠻⣿⣿⣿⣿⣿⣿⠟⠋⠉⠙⠻⣿⣿⣿⣿⣿⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⣿⣿⣿⡏⠀⠀⠀⠀⠀⠈⢻⣿⣿⡿⠁⠀⠀⠀⠀⠀⠸⣿⣿⣿⠏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠹⣿⣿⡇⠀⠀⠀⠀⠀⠀⣸⣿⣿⣧⠀⠀⠀⠀⠀⠀⢀⣿⣿⠟⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⠿⣿⣄⣀⠀⠀⣀⣴⣿⣿⣿⣿⣧⣀⡀⠀⢀⣀⣼⡿⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⠿⣿⣿⣿⣿⣿⣯⡀⠀⣸⣿⣿⣿⣿⣿⠿⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡀⢀⣤⣍⣙⠛⠛⠛⠿⠿⠛⠛⠛⣋⣩⣤⠀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣾⡇⡘⠿⣿⡏⢸⣿⣷⣶⢰⣶⣿⡇⣿⣿⡿⠃⢸⣦⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣾⣿⣿⠠⣿⣷⣶⠁⣌⣉⣛⠛⠘⣛⣋⡁⢨⣶⣶⣿⡸⣿⣿⣷⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⢀⣴⣶⣦⣄⣤⣾⣿⣿⣿⡿⢰⣮⡉⠛⢠⣿⣿⣿⣿⢸⣿⣿⣿⠈⠟⢋⣩⡄⣿⣿⣿⣿⣷⣄⣠⣴⣶⣦⡀⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⡿⠋⠀⠈⣿⣿⣿⣷⣶⣤⣤⣤⣤⣤⣤⣴⣶⣿⣿⣿⠁⠀⠙⢿⣿⣿⣿⣿⣿⣿⣿⣷⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠘⠿⠿⣿⣿⣿⣿⣯⠀⠀⠀⠀⠘⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠃⠀⠀⠀⠀⣿⣿⣿⣿⡿⠿⠟⠁⠀⠀⠀⠀⠀
      ⠀⠀⠀⠀⠀⠀⠀⠀⢻⣿⣿⣿⣿⠀⠀⠀⠀⠀⠈⠻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠃⠀⠀⠀⠀⠸⣿⣿⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀
            ⠀⠀⠀⠙⠛⠛⠋⠀⠀⠀⠀⠀⠀⠀⠀⠈⠙⠛⠛⠛⠛⠋⠉⠀⠀⠀⠀⠀⠀⠀⠀⠙⠛⠛⠋⠀⠀⠀
    ]]

		math.randomseed(os.time())

		local function pick_color()
			local colors = { "String", "Identify", "Keyword", "Number" }
			return colors[math.random(#colors)]
		end

		local header = theta.header
		header.val = vim.split(straw_hat_crew, "\n")
		header.opts = {
			position = "center",
			hl = pick_color(),
		}

		local mru_section = config.layout[4]
    local mru_title = mru_section.val[1]
		mru_title.val = "Recent Files " .. cdir
    local mru_files = mru_section.val[3]
    mru_files.val = function()
      return { mru(0, cdir, 5) }
    end

		local buttons = theta.buttons
		buttons.val = {
			{ type = "text", val = "Quick Links", opts = { hl = "SpecialComment", position = "center" } },
			{ type = "padding", val = 1 },
			dashboard.button("e", "  New file", "<cmd>ene<CR>"),
			dashboard.button("q", "󰅚  Quit", "<cmd>qa<CR>"),
		}
		buttons.position = "center"

		alpha.setup(config)
	end,
}

-- vim: ts=2 sts=2 sw=2 et
