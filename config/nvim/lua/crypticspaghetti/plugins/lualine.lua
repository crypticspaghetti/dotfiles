-- Status line for nvim

return {
	"nvim-lualine/lualine.nvim",
	opts = {
		options = {
			theme = "dracula",
		},
	},
}

-- vim: ts=2 sts=2 sw=2 et
