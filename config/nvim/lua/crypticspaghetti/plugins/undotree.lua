-- Branching undo history visualizer

return {
	"mbbill/undotree",
	keys = {
		{ "<leader>u", vim.cmd.UndotreeToggle, desc = "Toggle undo tree" },
	},
}

-- vim: ts=2 sts=2 sw=2 et
