return {
	{
		"williamboman/mason.nvim",
		lazy = false,
		config = true,
		opts = {
			ui = {
				icons = {
					package_installed = "",
					package_pending = "",
					package_uninstalled = "",
				},
			},
		},
	},
	{
		"WhoIsSethDaniel/mason-tool-installer.nvim",
		opts = {
			ensure_installed = {
				-- Formatters
				"stylua",
				"prettier",
				"erb-formatter",
				-- Linters
				"eslint_d",
				"stylelint",
				"pylint",
				-- Both
				"rubocop",
			},
		},
	},
}
