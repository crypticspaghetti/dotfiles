-- LSP related plugins and configuration

return {
	{
		"williamboman/mason-lspconfig.nvim",
		lazy = false,
		opts = {
			ensure_installed = {
				"lua_ls",
				"ts_ls",
				"ruby_lsp",
				"rubocop",
			},
			automatic_installation = true,
		},
	},
	{
		"neovim/nvim-lspconfig",
		event = { "BufReadPre", "BufNewFile" },
		dependencies = {
			"hrsh7th/cmp-nvim-lsp",
		},
		lazy = false,
		init = function()
			local capabilities = require("cmp_nvim_lsp").default_capabilities()

			local lspconfig = require("lspconfig")
			lspconfig.lua_ls.setup({
				capabilities = capabilities,
				settings = {
					Lua = {
						diagnostics = {
							globals = { "vim" },
						},
						completion = {
							callSnippet = "Replace",
						},
					},
				},
			})
			lspconfig.ts_ls.setup({
				capabilities = capabilities,
			})
			lspconfig.ruby_lsp.setup({
				capabilities = capabilities,
			})

			local signs = { Error = " ", Warn = " ", Hint = "󰠠 ", Info = " " }
			for type, icon in pairs(signs) do
				local hl = "DiagnosticSign" .. type
				vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
			end
		end,
		keys = {
			{
				"K",
				vim.lsp.buf.hover,
				mode = "n",
				desc = "Show documentation for what is under cursor",
			},
			{ "gd", vim.lsp.buf.definition, mode = "n", desc = "Jump to definition" },
			{ "gr", vim.lsp.buf.references, mode = "n", desc = "Show references" },
			{ "gf", vim.lsp.buf.format, mode = "n", desc = "Format file using fallback" },
			{
				"<leader>e",
				vim.diagnostic.open_float,
				mode = "n",
				desc = "Open a floating pane with diagnostics",
			},
			{
				"<leader>ca",
				vim.lsp.buf.code_action,
				mode = { "n", "v" },
				desc = "Display code actions available for line or selection",
			},
			{ "rn", vim.lsp.buf.rename, mode = { "n", "v" }, desc = "Rename variable" },
		},
	},
}

-- vim: ts=2 sts=2 sw=2 et
