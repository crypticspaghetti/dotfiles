-- Navigate tmux panes even while in nvim

return {
	"christoomey/vim-tmux-navigator",
	cmd = {
		"TmuxNavigateLeft",
		"TmuxNavigateDown",
		"TmuxNavigateUp",
		"TmuxNavigateRight",
		"TmuxNavigatePrevious",
	},
	keys = {
		{ "<C-h>", "<cmd><C-U>TmuxNavigateLeft<cr>", desc = "Navigate to tmux pane to the left" },
		{ "<C-j>", "<cmd><C-U>TmuxNavigateDown<cr>", desc = "Navigate to tmux pane underneath" },
		{ "<C-k>", "<cmd><C-U>TmuxNavigateUp<cr>", desc = "Navigate to tmux pane above" },
		{ "<C-l>", "<cmd><C-U>TmuxNavigateRight<cr>", desc = "Navigate to tmux pane to the right" },
		{ "<C-\\>", "<cmd><C-U>TmuxNavigatePrevious<cr>", desc = "Navigate to the previous tmux pane" },
	},
}

-- vim: ts=2 sts=2 sw=2 et
