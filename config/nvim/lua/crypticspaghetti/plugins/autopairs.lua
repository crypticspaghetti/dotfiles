-- automatically pairs parenthesi, brackets, quotes, etc.
-- also moves the end of the pair down two lines when <CR> is used between them

return {
	"windwp/nvim-autopairs",
	event = "InsertEnter",
	config = true,
}

-- vim: ts=2 sts=2 sw=2 et
