vim.g.mapleader = " "

local keymap = vim.keymap

-- Move lines in Visual mo
keymap.set("v", "J", ":m '>+1<cr>gv=gv")
keymap.set("v", "K", ":m '<-2<cr>gv=gv")

-- Attempt to keep cursor in middle line as we page-up/down
keymap.set("n", "<C-u>", "<C-u>zz")
keymap.set("n", "<C-d>", "<C-d>zz")

-- Attempt to keep cursor in middle line as we go through search results
keymap.set("n", "n", "nzzzv")
keymap.set("n", "N", "Nzzzv")

-- Paste over selected text while keeping old buffer
keymap.set("x", "<leader>p", '"_dP', { desc = "Paste over selection while keeping old buffer" })

-- Yank into system clipboard - credit: asbjornHaland
keymap.set({ "n", "v" }, "<leader>y", '"+y', { desc = "Yank into system clipboard" })
keymap.set("n", "<leader>Y", '"+Y', { desc = "Yank line into system clipboard" })

-- Send the deleted to the Shadow Realm instead of paste buffer
keymap.set({ "n", "v" }, "<leader>d", '"_d', { desc = "Delete outside of the paste buffer" })

keymap.set(
	"n",
	"<leader>s",
	":%s/\\<<C-r><C-w>\\>/<C-r><C-w>/gI<Left><Left><Left>",
	{ desc = "Replace all instances of word" }
)

-- Set current file as executable
keymap.set("n", "<leader>x", "<cmd>!chmod +x %<cr>", { silent = true, desc = "Make current file executable" })

-- Tab management
keymap.set("n", "<leader>to", "<cmd>tabnew<cr>", { desc = "Open new tab" })
keymap.set("n", "<leader>tx", "<cmd>tabclose<cr>", { desc = "Close current tab" })
keymap.set("n", "<leader>tn", "<cmd>tabn<cr>", { desc = "Go to next tab" })
keymap.set("n", "<leader>tp", "<cmd>tabp<cr>", { desc = "Go to previous tab" })
keymap.set("n", "<leader>tf", "<cmd>tabnew %<cr>", { desc = "Open current buffer in new tab" })

--  ts=2 sts=2 sw=2 et
