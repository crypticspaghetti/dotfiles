local opt = vim.opt

opt.termguicolors = true

opt.number = true
opt.relativenumber = true
opt.cursorline = true

opt.expandtab = true
opt.smartindent = true
opt.tabstop = 2
opt.softtabstop = 2
opt.shiftwidth = 2

opt.wrap = false

opt.swapfile = false
opt.backup = false
opt.undodir = os.getenv("XDG_CONFIG_HOME") .. "/nvim/undodir"
opt.undofile = true

opt.hlsearch = false
opt.incsearch = true
-- Ignore casing in search unless \C or more than one capital letter involved
opt.ignorecase = true
opt.smartcase = true

opt.scrolloff = 10
opt.signcolumn = "yes"
opt.isfname:append("@-@")

opt.updatetime = 50

opt.colorcolumn = "100"

opt.clipboard = "unnamedplus"

opt.splitright = true
opt.splitbelow = true
-- Using a status line plugin, no need for this
opt.showmode = false

-- vim: ts=2 sts=2 sw=2 et
