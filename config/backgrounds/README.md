# Wallpaper Sources

While hosting the wallpaper images here, I will link the images in this directory to where I got
them. I will try to link directly to the artist source, but sometimes I won't be able to. In these
cases I will link to the URL I got the image from.

---

- Mizore Shirayuki
[![Mizore](mizore.jpg)](https://wall.alphacoders.com/big.php?i=118917)
- Tohka Yatogami
[![Tohka](tohka.jpg)](https://wall.alphacoders.com/big.php?i=557188)
- Seele Vollerei
[![Seele](seele.png)](https://wall.alphacoders.com/big.php?i=1183595)
