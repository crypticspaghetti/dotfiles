#!/bin/sh

# one-liner courtesy https://stackoverflow.com/a/246128
DOTFILES=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

git submodule update

# TODO extract into a function that loops through each folder
ln -s "$DOTFILES/config/backgrounds" "$HOME/.config"
ln -s "$DOTFILES/config/bat" "$HOME/.config"
ln -s "$DOTFILES/config/hypr" "$HOME/.config"
ln -s "$DOTFILES/config/kitty" "$HOME/.config"
ln -s "$DOTFILES/config/lazygit" "$HOME/.config"
ln -s "$DOTFILES/config/newsboat" "$HOME/.config"
ln -s "$DOTFILES/config/nvim" "$HOME/.config"
ln -s "$DOTFILES/config/rofi" "$HOME/.config"
ln -s "$DOTFILES/config/tmux" "$HOME/.config"
ln -s "$DOTFILES/config/vesktop" "$HOME/.config"
ln -s "$DOTFILES/config/waybar" "$HOME/.config"
ln -s "$DOTFILES/config/yazi" "$HOME/.config"
ln -s "$DOTFILES/config/zsh" "$HOME/.config"
ln -s "$DOTFILES/scripts" "$HOME/.scripts"

# TODO extract into a function that loops through each file
ln -sf "$DOTFILES/config/starship.toml" "$HOME/.config/"
ln -sf "$DOTFILES/config/user-dirs.dirs" "$HOME/.config/"
ln -sf "$DOTFILES/scripts/fzf-git.sh/fzf-git.sh" "$HOME/.scripts/fzf-git.sh"
ln -sf "$DOTFILES/.zshenv" "$HOME/"
